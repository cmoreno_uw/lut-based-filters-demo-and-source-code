/***************************************************************************

    File accompanying the DAC2017 submission "Fast and Energy-Efficient 
    Digital Filters for Signal Conditioning in Low-Power Microcontrollers"

    Author:  Carlos Moreno (University of Waterloo, Canada)


    Copyright (C) 2017, Carlos Moreno
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    THE ABOVE DISCLAIMER INCLUDES ANY LIABILITY DIRECTLY OR INDIRECTLY DERIVING FROM 
    THE USE OF THE FILTERS THAT THIS SOFTWARE PRODUCES, OR THE USE OF ANY OUTPUT OR 
    BY-PRODUCT RESULTING FROM THE USE OF THIS SOFTWARE, INCLUDING, BUT NOT LIMITED 
    TO, HEADER FILES, TESTING PROTOCOL PROGRAMS, etc.

    The views and conclusions contained in the software and documentation are those
    of the author and should not be interpreted as representing official policies,
    either expressed or implied, of the University of Waterloo or any of its 
    departments.


    Description:

    This program demonstrate a LUT-based 12-bit multiplication through 
    a triple 4-bit LUTs configuration.  It computes ax through the LUTs 
    and also with floating-point arithmetic to determine the quantization 
    error; this is done for a positive and a negative coefficient (0.3 
    and -0.3). All 4096 possible input values are tested, computing the 
    quantization error for each and reporting error statistics at the 
    end.

 ***************************************************************************/
#include <iostream>
#include <cmath>
using namespace std;

#include <inttypes.h>

uint8_t  LUT_0p3_low[16]  = {0,0,1,1,1,2,2,2,2,3,3,3,4,4,4,5};
uint8_t  LUT_0p3_mid[16]  = {0,5,10,14,19,24,29,34,38,43,48,53,58,62,67,72};
uint16_t LUT_0p3_high[16] = {0,77,154,230,307,384,461,538,
                             3482,3558,3635,3712,3789,3866,3942,4019};

uint8_t  LUT_m0p3_low[16]  = {0,0,1,1,1,2,2,2,2,3,3,3,4,4,4,5};
uint8_t  LUT_m0p3_mid[16]  = {0,5,10,14,19,24,29,34,38,43,48,53,58,62,67,72};
uint16_t LUT_m0p3_high[16] = {0,4019,3942,3866,3789,3712,3635,3558,
                              614,538,461,384,307,230,154,77};

inline uint8_t hi4 (uint16_t x)
{
    return x >> 8;
}

inline uint8_t mid4 (uint16_t x)
{
    return (x & 0xF0) >> 4;
}

inline uint8_t lo4 (uint16_t x)
{
    return x & 0xF;
}

int main()
{
    double max_err = 0, sum_err = 0, sum_err_square = 0;

    for (int16_t k = -2048; k < 2048; ++k)
    {
        const int16_t x = k & 0xFFF;
        int16_t ax = (LUT_0p3_high[hi4(x)] + LUT_0p3_mid[mid4(x)] + LUT_0p3_low[lo4(x)]) & 0xFFF;
        const double err = (ax < 2048 ? ax - 0.3*k : (ax - 4096) - 0.3*k);  // k preserves the sign
        sum_err += err;
        sum_err_square += err*err;
        if (abs(err) > max_err)
        {
            max_err = err;
        }

        int16_t ax2 = (LUT_m0p3_high[hi4(x)] - LUT_m0p3_mid[mid4(x)] - LUT_m0p3_low[lo4(x)]) & 0xFFF;
        const double err2 = (ax2 < 2048 ? ax2 + 0.3*k : (ax2 - 4096) + 0.3*k);  // k preserves the sign
        sum_err += err2;
        sum_err_square += err2*err2;
        if (abs(err2) > max_err)
        {
            max_err = err2;
        }
    }

    const double mean = sum_err / 8192;
    const double var = (sum_err_square - 2*mean*sum_err + 8192*mean*mean) / 8191;

    cout << "Mean error: " << mean << ", std dev: " << sqrt(var)
         << "\nMax error: " << max_err << endl;

    return 0;
}
