/***************************************************************************

    File accompanying the DAC2017 submission "Fast and Energy-Efficient 
    Digital Filters for Signal Conditioning in Low-Power Microcontrollers"

    Author:  Carlos Moreno (University of Waterloo, Canada)


    Copyright (C) 2017, Carlos Moreno
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    THE ABOVE DISCLAIMER INCLUDES ANY LIABILITY DIRECTLY OR INDIRECTLY DERIVING FROM 
    THE USE OF THE FILTERS THAT THIS SOFTWARE PRODUCES, OR THE USE OF ANY OUTPUT OR 
    BY-PRODUCT RESULTING FROM THE USE OF THIS SOFTWARE, INCLUDING, BUT NOT LIMITED 
    TO, HEADER FILES, TESTING PROTOCOL PROGRAMS, etc.

    The views and conclusions contained in the software and documentation are those
    of the author and should not be interpreted as representing official policies,
    either expressed or implied, of the University of Waterloo or any of its 
    departments.


    Description:

    This program generates the LUTs for all coefficients arranged to have 
    impulse responses fully adjacent, to implement the input-centric 
    filtering method. 

    Related file: test-LUT--h.c++ containing a test program for the 
    input-centric method (the testing covers correctness of this program).

 ***************************************************************************/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
using namespace std;

#include <inttypes.h>

int main (int argc, const char * arg[])
{
    if (argc < 3) { cout << "Usage: " << arg[0] << " <header filename (without the .h)> <impulse response (space-separated coefficients)>" << endl; return 0; }

    const int h_length = argc - 2;
    string filename = arg[1];

    vector< vector<uint8_t> > LUT_low (256, vector<uint8_t>(h_length));
    vector< vector<uint16_t> > LUT_high (256, vector<uint16_t>(h_length));

    for (int k = 0; k < h_length; ++k)
    {
        const double a = atof(arg[k+2]);

        for (int i = 0; i < 256; ++i)
        {
             LUT_low[i][k] = (a >= 0 ? static_cast<uint16_t>(a*i + 0.5) & 0xFF : static_cast<uint8_t>(-a*i + 0.5));
        }

        for (int i = 0; i < 128; ++i)
        {
            LUT_high[i][k] = static_cast<uint16_t>(a >= 0 ? a*(i << 8) + 0.5 : a*(i << 8) - 0.5);
        }

        for (int i = 128; i < 256; ++i)
        {
            const double val = a*(i << 8) - a*(1<<16);
            LUT_high[i][k] = static_cast<uint16_t>(a >= 0 ? val - 0.5 : val + 0.5);
        }
    }

    ofstream file ((filename + ".h").c_str());
    if (file)
    {
        file << "#ifndef " << filename << "_H\n"
                "#define " << filename << "\n\n"
                "#include <inttypes.h>\n\n"

                "uint8_t LUT_" << filename << "_low[256*" << h_length << "] =\n{";

        for (int x = 0; x < 255; ++x)
        {
            for (int k = 0; k < h_length; ++k)
            {
                file << static_cast<unsigned int>(LUT_low[x][k]) << ", ";
            }
            file << '\n';
        }
        for (int k = 0; k < (h_length-1); ++k)
        {
            file << static_cast<unsigned int>(LUT_low[255][k]) << ", ";
        }
        file << static_cast<unsigned int>(LUT_low[255][h_length-1]) << "};\n\n"
                "uint16_t LUT_" << filename << "_high[256*" << h_length << "] =\n{";

        for (int x = 0; x < 255; ++x)
        {
            for (int k = 0; k < h_length; ++k)
            {
                file << static_cast<unsigned int>(LUT_high[x][k]) << ", ";
            }
            file << '\n';
        }
        for (int k = 0; k < (h_length-1); ++k)
        {
            file << static_cast<unsigned int>(LUT_high[255][k]) << ", ";
        }
        file << static_cast<unsigned int>(LUT_high[255][h_length-1]) << "};\n\n"
                "#endif\n\n";
    }
    else
    {
        cerr << "ERROR: could not create file " << filename << ".h" << endl;
        return 1;
    }

    return 0;
}
