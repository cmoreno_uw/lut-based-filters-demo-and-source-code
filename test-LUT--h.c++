/***************************************************************************

    File accompanying the DAC2017 submission "Fast and Energy-Efficient 
    Digital Filters for Signal Conditioning in Low-Power Microcontrollers"

    Author:  Carlos Moreno (University of Waterloo, Canada)


    Copyright (C) 2017, Carlos Moreno
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    THE ABOVE DISCLAIMER INCLUDES ANY LIABILITY DIRECTLY OR INDIRECTLY DERIVING FROM 
    THE USE OF THE FILTERS THAT THIS SOFTWARE PRODUCES, OR THE USE OF ANY OUTPUT OR 
    BY-PRODUCT RESULTING FROM THE USE OF THIS SOFTWARE, INCLUDING, BUT NOT LIMITED 
    TO, HEADER FILES, TESTING PROTOCOL PROGRAMS, etc.

    The views and conclusions contained in the software and documentation are those
    of the author and should not be interpreted as representing official policies,
    either expressed or implied, of the University of Waterloo or any of its 
    departments.


    Description:

    This program tests the input-centric filtering method. It creates a random 
    sequence of 7 input values (the input signal), and computes the first sample 
    of the output, both with the input-centric method and through direct 
    floating-point based computation of the output sample (Equation(1) in the 
    submission).  The quantization error is computed and error statistics are 
    displayed at the end.

    Related files: 
        generate_LUT--h.c++ to generate the #include file with the LUT for the 
        input-centric method

        test-h.h  (filter used for this test)

 ****************************************************************************************/
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cmath>
using namespace std;

#include <inttypes.h>

#include "test-h.h"
const int L = 7;

inline uint8_t hi8 (uint16_t x)
{
    return x >> 8;
}

inline uint8_t lo8 (uint16_t x)
{
    return x & 0xFF;
}

int main()
{
    srand48 (time(NULL));
    const int N = 1000000;

    vector<int16_t> x(L), x_max_err(L);
    int16_t y_max_err = 0;
    double max_err = 0, mean_err = 0, std_dev = 0;

    for (int i = 0; i < N; ++i)
    {
        for (int k = 0; k < L; ++k)
        {
            x[k] = -23300 + static_cast<int32_t>(drand48()*46600);
            // To avoid overflow --- sum{abs(h(n))} is 1.4 for this filter
            // so it can produce an output up to 1.4 x max input value.
            // Input values in the range +- 32768/1.4  (with a comfortable 
            // margin to account for quantization error)
        }

        vector<int16_t> y(2*L);
        for (int n = 0; n < L; ++n)
        {
            uint16_t * out = reinterpret_cast<uint16_t *>(&y[n]);
            uint16_t * h_high = &LUT_test_h_high[L * hi8(x[n])];
            uint8_t  * h_low  = &LUT_test_h_low[L * lo8(x[n])];

            *out   += *h_high++;
            *out++ += *h_low++;
            *out   += *h_high++;
            *out++ -= *h_low++;
            *out   += *h_high++;
            *out++ += *h_low++;
            *out   += *h_high++;
            *out++ += *h_low++;
            *out   += *h_high++;
            *out++ += *h_low++;
            *out   += *h_high++;
            *out++ -= *h_low++;
            *out   += *h_high++;
            *out++ += *h_low++;
        }

        const double actual_output = 0.1*x[0] - 0.2*x[1] + 0.15*x[2] + 0.5*x[3] + 0.15*x[4] - 0.2*x[5] + 0.1*x[6];
        const double err = y[L-1] - actual_output;

        mean_err += err;
        std_dev += err*err;
        if (abs(err) > max_err)
        {
            max_err = abs(err);
            x_max_err.swap (x);
            y_max_err = y[L-1];
        }
    }

    const double mean = mean_err / N;
    const double var = (std_dev - 2*mean*mean_err + N*mean*mean) / (N-1);

    cout << "Mean error: " << mean << ", std dev: " << sqrt(var)
         << "\nMax error: " << max_err 
         << "\nWith input: ";
    copy (x_max_err.begin(), x_max_err.end(), ostream_iterator<int16_t>(cout," "));
    cout << "\n\nComputed output: " << y_max_err << ", actual output: " << (0.1*x_max_err[0] - 0.2*x_max_err[1] + 0.15*x_max_err[2] + 0.5*x_max_err[3] + 0.15*x_max_err[4] - 0.2*x_max_err[5] + 0.1*x_max_err[6]) << endl;

    return 0;
}
