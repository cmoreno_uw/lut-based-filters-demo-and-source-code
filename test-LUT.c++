/***************************************************************************

    File accompanying the DAC2017 submission "Fast and Energy-Efficient 
    Digital Filters for Signal Conditioning in Low-Power Microcontrollers"

    Author:  Carlos Moreno (University of Waterloo, Canada)


    Copyright (C) 2017, Carlos Moreno
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    THE ABOVE DISCLAIMER INCLUDES ANY LIABILITY DIRECTLY OR INDIRECTLY DERIVING FROM 
    THE USE OF THE FILTERS THAT THIS SOFTWARE PRODUCES, OR THE USE OF ANY OUTPUT OR 
    BY-PRODUCT RESULTING FROM THE USE OF THIS SOFTWARE, INCLUDING, BUT NOT LIMITED 
    TO, HEADER FILES, TESTING PROTOCOL PROGRAMS, etc.

    The views and conclusions contained in the software and documentation are those
    of the author and should not be interpreted as representing official policies,
    either expressed or implied, of the University of Waterloo or any of its 
    departments.


    Description:

    This program tests/validates the use of individual LUTs. It tests 
    multiplication times positive and negative coefficients (0.156 and 
    -0.27341) exhaustively testing the input (i.e., testing all 65536 
    possible input values).  For each value, the result is computed 
    through the LUTs and also through direct floating-point multiplication 
    to verify the result; in particular, to determine the quantization 
    error. At the end, statistics on the error are reported.

    Related files: 
        generate-LUT.c++ to generate the #include files with the LUT 
        definition/initialization.

        lut_0p156.h and lut_m0p27341.h  (LUTs used for the test)

 ***************************************************************************/
#include <iostream>
#include <cmath>
using namespace std;

#include "lut_0p156.h"
#include "lut_m0p27341.h"

inline uint8_t hi8 (uint16_t x)
{
    return x >> 8;
}

inline uint8_t lo8 (uint16_t x)
{
    return x & 0xFF;
}

int main()
{
    double max_err = 0, sum_err = 0, sum_err_square = 0;

    for (int32_t k = -32768; k < 32768; ++k)
    {
        const int16_t x = k;
        int16_t ax = LUT_0p156_high[hi8(x)] + LUT_0p156_low[lo8(x)];
        const double err = ax - 0.156*x;
        sum_err += err;
        sum_err_square += err*err;
        if (abs(err) > max_err)
        {
            max_err = err;
        }

        int16_t anegx = LUT_m0p27341_high[hi8(x)] - LUT_m0p27341_low[lo8(x)];
        const double err2 = anegx + 0.27341*x;
        sum_err += err2;
        sum_err_square += err2*err2;
        if (abs(err2) > max_err)
        {
            max_err = err2;
        }
    }

    const double mean = sum_err / 65536;
    const double var = (sum_err_square - 2*mean*sum_err + 65536*mean*mean) / 65535;

    cout << "Mean error: " << mean << ", std dev: " << sqrt(var)
         << "\nMax error: " << max_err << endl;

    return 0;
}
