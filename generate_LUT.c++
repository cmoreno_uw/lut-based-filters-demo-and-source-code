/***************************************************************************

    File accompanying the DAC2017 submission "Fast and Energy-Efficient 
    Digital Filters for Signal Conditioning in Low-Power Microcontrollers"

    Author:  Carlos Moreno (University of Waterloo, Canada)


    Copyright (C) 2017, Carlos Moreno
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    THE ABOVE DISCLAIMER INCLUDES ANY LIABILITY DIRECTLY OR INDIRECTLY DERIVING FROM 
    THE USE OF THE FILTERS THAT THIS SOFTWARE PRODUCES, OR THE USE OF ANY OUTPUT OR 
    BY-PRODUCT RESULTING FROM THE USE OF THIS SOFTWARE, INCLUDING, BUT NOT LIMITED 
    TO, HEADER FILES, TESTING PROTOCOL PROGRAMS, etc.

    The views and conclusions contained in the software and documentation are those
    of the author and should not be interpreted as representing official policies,
    either expressed or implied, of the University of Waterloo or any of its 
    departments.


    Description:

    This program generates a header file (lut_xxxx.h) defining the LUTs 
    for a given coefficient, assuming 16-bit input and output.  The file 
    name convention is:  for a coefficient 0.x, filename is lut_0px.h; 
    for a negative coefficient -0.x, filename is lut_m0px

    Related file: test-LUT.c++ containing a test program using LUTs for 
    0.156 and -0.27341

 ***************************************************************************/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
using namespace std;

#include <inttypes.h>

int main (int argc, const char * arg[])
{
    if (argc < 2) { cout << "Usage: " << arg[0] << " <coefficient>" << endl; return 0; }

    const double a = atof(arg[1]);
    string a_str (arg[1]);
    if (a_str[0] == '-')
    {
        a_str[0] = 'm';
    }
    const string::size_type point = a_str.find('.');

    string filename = "lut_" + a_str + ".h";
    filename[4+point] = 'p';

    string guard = "LUT_" + a_str + "_H";
    guard[4+point] = 'P';

    a_str[point]='p';

    ofstream file (filename.c_str());
    if (file)
    {
        file << "#ifndef " << guard << "\n"
                "#define " << guard << "\n\n"
                "#include <inttypes.h>\n\n"

                "uint8_t LUT_" << a_str << "_low[256] =\n{";

        for (int i = 0; i < 255; ++i)
        {
            if (i > 0 && i % 16 == 0)
            {
                file << '\n';
            }
            file << (a >= 0 ? static_cast<uint16_t>(a*i + 0.5) & 0xFF : static_cast<uint8_t>(-a*i + 0.5)) << ", ";
        }

        file << (a >= 0 ? static_cast<uint16_t>(a*255 + 0.5) & 0xFF : static_cast<uint8_t>(-a*255 + 0.5)) << "};\n\n"
                "uint16_t LUT_" << a_str << "_high[256] =\n{";

        for (int i = 0; i < 128; ++i)
        {
            if (i > 0 && i % 16 == 0)
            {
                file << '\n';
            }
            file << static_cast<uint16_t>(a >= 0 ? a*(i << 8) + 0.5 : a*(i << 8) - 0.5) << ", ";
        }

        for (int i = 128; i < 255; ++i)
        {
            if (i > 0 && i % 16 == 0)
            {
                file << '\n';
            }
            const double val = a*(i << 8) - a*(1<<16);
            file << static_cast<uint16_t>(a >= 0 ? val - 0.5 : val + 0.5) << ", ";
        }

        const double val = a*(255 << 8) - a*(1<<16);
        file << static_cast<uint16_t>(a >= 0 ? val - 0.5 : val + 0.5) << "};\n\n"
                "#endif\n\n";
    }
    else
    {
        cerr << "ERROR: could not create file " << filename << endl;
        return 1;
    }

    return 0;
}
